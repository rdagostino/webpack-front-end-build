const path = require('path');
const env = process.env.NODE_ENV || 'development';
const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

// Plugins
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = {
	devtool: (env === 'development') ? 'cheap-eval-source-map' : '',
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		port: 9000
	},
	entry: {
		global: path.resolve(__dirname, './src/js/global.js'),
		pcg: path.resolve(__dirname, './src/scss/_boilerplate/global.scss')
	},
	module: {
		rules: [{
			test: /\.(js|jsx)$/,
			exclude: /node_modules/,
			include: path.resolve(__dirname, './src/js'),
			enforce: 'pre',
			use: {
	          loader: 'eslint-loader'
	        }
		},
		{
			test: /\.(js|jsx)$/,
			exclude: /node_modules/,
			include: path.resolve(__dirname, './src/js'),
			resolve: { extensions: ['.js', '.jsx']},
			use: {
	          loader: 'babel-loader'
	        }
		},
		{
			test: /\.scss$/,
			include: path.resolve(__dirname, './src/scss'),
			use: [
				MiniCssExtractPlugin.loader,
				{
					loader: 'css-loader',
					options: { 
						sourceMap: true,
						minimize: (env === 'production')
					}
				},
				{
					loader: 'postcss-loader',
					options: {
						sourceMap: true,
						plugins: function () {
		                    return [autoprefixer];
		                }
					}
				},
				{
					loader: 'resolve-url-loader'
				},
				{
					loader:'sass-loader',
					options: {
						sourceMap: true,
						includePaths: ['./src/scss/_shared']
					}
				}
			]
		},
		{
			test: /\.(png|svg|jpg|jpeg|gif)$/,
			use: [{
				loader: 'file-loader',
				options: {
					name: '[name].[ext]',
					outputPath: 'images'
				}
			}]
		},
		{
			test: /\.(woff|woff2|eot|ttf|otf|svg)$/,
			use: [{
				loader: 'file-loader',
				options: {
					name: '[name].[ext]',
					outputPath: 'fonts'
				}
			}]
		}]
	},
	output: {
	    path: path.resolve(__dirname, 'dist'),
	    filename: '[name].js',
	},
	plugins: [
		// Sets the environment, so plugins can adjust to the correct environment
		// Default value is 'development'
		new webpack.DefinePlugin( {
	    	'process.env': {
	        	NODE_ENV: JSON.stringify(env),
			},
		}),

		// Converts [name].js to an actual css file, [name].css
		new MiniCssExtractPlugin({
			filename: '[name].css'
		}),

		// Clears out the dist directory before each build
		new CleanWebpackPlugin(['dist']),

		// SCSS linter
		// Linting configuration is stored in './stylelintrc'
		new StyleLintPlugin({
			configFile: path.resolve(__dirname, './.stylelintrc'),
			context: path.resolve(__dirname, './src/scss'),
			formatter: require('stylelint-formatter-pretty'),
	        emitErrors: true,
	        failOnError: false,
	        quiet: false,
	        syntax: 'scss'
		})
    ]
};