### Boilerplate ####

Clone this directory when creating a new brand to ensure common consistency between brand in structure and naming conventions for SASS, images and fonts.

When adding new assets to a brand that is cloned off this folder, please ensure that _boilerplate is updated. PR's will fail if this is not followed.