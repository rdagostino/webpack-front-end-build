### Shared ####

This directory is specific to default site styling and structure. All css property values will be set in variables and assigned the !default flag and are to be overwritten in the brand variables file.