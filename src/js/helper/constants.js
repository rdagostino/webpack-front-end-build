/*
 * Constants file
 *
 * This is a helper file to set up global properties to be used in
 * the various JS files across the site.
 */
export default class Constants {
	static regex = {
		phone: /^\(?\d{3}\)?[- ]?\d{3}[- ]?\d{4}$/g,
		email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g,
	}
}