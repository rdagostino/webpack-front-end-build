import Header from './components/header';
import Footer from './components/footer';

const siteHeader = new Header();
const siteFooter = new Footer();

siteHeader.load();
siteFooter.load();