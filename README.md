# Front-end Build Process

The foundation css, javascript, and images will be housed here. The build will be using Node & Webpack. Webpack will be a dependency within this project so the only dependency a developer will need to have installed on their machine is NodeJS.

## Build

### Quickstart
If you want to just build out the static assets, run the below script. It'll install dependencies & perform a base build of static assets.
```
npm prune && npm install && npm run build
```

### Local
Install node modules if you haven't done so already. It's best practice to do this every time you pull down new code, esepcially if you see a change to `package.json`.
```
npm prune && npm install
```

Start the webpack build. This will also start the watch process for any changes in the codebase.
```
npm start
```

### Deployment
When doing a deployment to the upper environments, we'll want to use the below build script so we have cleaner & minified code.
```
npm run build:deploy
```